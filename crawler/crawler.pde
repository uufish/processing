void setup(){
  frameRate(14);
  size(1000, 800);
  initClass();  
}

color col = #59b9c6;

void draw(){
  background(#fffffc);
  releaseBetaClass();
  releaseAlphaClass();
}
    
Alpha clock;    
ArrayList<Beta> crawler;

void initClass() {
  clock = new Alpha(width/2, height/2);    
  crawler = new ArrayList<Beta>();
  for (int i = 0; i < 7; ++i) {
    crawler.add(new Beta(random(width), random(height)));
  }
}
    
    
void releaseBetaClass() {
  for (Beta c:crawler) {
    c.releaseBetaObject();
  }
}

void releaseAlphaClass() {
  clock.releaseAlphaObject();
}