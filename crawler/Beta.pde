class Beta {
  
  float x[];
  float xx;
  float y[];
  float yy;
  float segLength = 8;
  float pct  = 0;
  float exp  = 2.0;
  float step = 0.001;
 
  Beta(float tempx, float tempy) {
    x    = new float[10];
    x[0] = tempx;
    y    = new float[10];
    y[0] = tempy;
    initPathOfCrawler();
  }
      
  void initPathOfCrawler() { 
    xx = random( 10, width  - 10 );
    yy = random( 10, height - 10 );
  }
  
  void releaseBetaObject() {
    pct += step;
    
    if (pct < 1.0) {
        x[0] += (xx - x[0]) * pct;
        y[0] += (yy - y[0]) * pow( pct, exp );
    }
     
    float rot[] = new float[x.length];
     
    for(int i = 1; i < x.length; i++) {
      rot[i]= atan2(y[i-1] - y[i], x[i-1] - x[i]);
      
      x[i] = x[i-1] - cos( rot[i] ) * segLength;
      y[i] = y[i-1] - sin( rot[i] ) * segLength;
    } 
        
    rot[0]= atan2(y[1] - y[0], x[1] - x[0]);
  
    if(dist(x[0], y[0], xx, yy) < 100) {
      pct = 0;
      xx = random(10, width  - 10);
      yy = random(10, height - 10);
    }
    
    stroke(col, 270);
    noFill();
    strokeWeight(1.4);
    
    pushMatrix();
    translate(x[0], y[0]);
    rotate(rot[1]); 
    triangle(16, 0, 6, 6, 6, -6);
    line(2, -3, 2, 3);
    popMatrix();
    
    pushMatrix();
    translate(x[2], y[2]);
    rotate(rot[2]); 
    line(0, -3, 0, 3);
    popMatrix();
    
    pushMatrix();
    translate(x[3], y[3]);
    rotate(rot[3]); 
    line(0, -2, 0, 2);
    popMatrix();
    
    pushMatrix();
    translate(x[4], y[4]);
    rotate(rot[4]); 
    line(0, -6, 0, 6);
    popMatrix();
    
    pushMatrix();
    translate(x[5], y[5]);
    rotate(rot[5]); 
    line(0, -5, 0, 5);
    popMatrix();
    
    pushMatrix();
    translate(x[7], y[7]);
    rotate(rot[7]); 
    line(0, -2, 0, 2);
    popMatrix();
    
    pushMatrix();
    translate(x[1], y[1]);
    rotate(rot[1]); 
    triangle(0, 12, -7, 17, 0, 10);
    triangle(0, -12, -7, -17, 0, -10);
    popMatrix();
 
    pushMatrix();
    translate(x[9], y[9]);
    rotate(rot[9]); 
    line(0, 0, -3,  3);
    line(0, 0, -3, -3);
    popMatrix();
  }
}