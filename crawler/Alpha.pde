class Alpha { 
  float x;
  float y;
  int hour;
  int min;
  int sec;
  boolean reversal = false;

  Alpha(float tempx, float tempy) {
    x = tempx;
    y = tempy;
  }

  void releaseAlphaObject() {
    strokeWeight(1.4);
    stroke (col, 100);
    noFill();

    ellipse(x, y, 50, 50);
    fill(col, 25);
    noStroke();
    ellipse(x, y, 180, 180);
      
    stroke(col, 100);
    noFill();
    
    strokeWeight(1.4);
    arc(x, y, 140, 140, QUARTER_PI, TWO_PI +QUARTER_PI -radians(360/24 *hour()));
 
    strokeWeight(1.4);
    arc(x, y, 150, 150, HALF_PI +QUARTER_PI, TWO_PI +HALF_PI +QUARTER_PI -radians(360/60 *minute()));
 
    strokeWeight(1.4);
    pushMatrix();
    translate(x, y);
    rotate(radians(360/60 * second()) +QUARTER_PI);
    triangle(-5, 55, 0, 45, 5, 55);
    popMatrix();
    
    fill(col, 270);
    textSize(25);
    textAlign(CENTER);
    }
 }