void setup () {
  size(500, 500);
  frameRate(20);
  background(#fbfaf5);
  smooth();
  stroke(#83ccd2,80);
  strokeWeight(0.2);
  x[0] = width / 2;
  y[0] = height / 2;
  for (int i=1; i<num; ++i){
    x[i] = x[i-1];
    x[i] += (random(0,1) < 0.5) ? random(0, 5) : -1 * random(0, 5);
    
    y[i] = y[i-1];
    y[i] += (random(0,1) < 0.5) ? random(0, 5) : -1 * random(0, 5);
  }
}

int num = 8000;
float[] x = new float[num];
float[] y = new float[num];
float rot = 0;

void draw () {
  fill(#fbfaf5, 80);
  rect(0, 0, width, height);
  for (int i=1; i<num; ++i) {
    if (random(0,1)<0.01) {
      x[i] = x[i-1];
      x[i] += (random(0,1)<0.5) ? random(0, 50) : -1*random(0, 50);
    }
    if (random(0,1)<0.01) {
      y[i] = y[i-1];
      y[i] += (random(0,1)<0.5) ? random(0, 50) : -1*random(0, 50);
    }
    fill(#83ccd2, 20);
    ellipse(x[i], y[i], 5, 5);
    for (int k=i+1; k<num; ++k) {
      if (random(0,1) < 0.8) break;
      line(x[i],y[i],x[k],y[k]);
    }
  }
}