int num = 140;

float[]
  s  = new float [num],
  x  = new float [num],
  dx = new float [num], 
  y  = new float [num], 
  dy = new float [num];

void setup() {
  size(500, 500);
  frameRate(20);
  stroke(250);
  fill(255);
  init(num);
}

void init(int i) {
  i--;
  x[i] = int(random(0, width));
  y[i] = int(random(0, height));
  s[i] = int(random(1, 4));
  dx[i] = random(1) < 0.5 ? random(2) : -random(2);
  dy[i] = random(1) < 0.5 ? random(2) : -random(2);
  if (0 < i) init(i);
}

void draw() {
  background(200);
  drawEllipse(num);
}

void drawEllipse(int i) {
  i--;
  x[i] += dx[i] *= x[i] < 0 || width  < x[i] ? -1 : 1 ;
  y[i] += dy[i] *= y[i] < 0 || height < y[i] ? -1 : 1 ;
  ellipse
    (x[i], y[i], s[i], s[i]);
  if (0 < i) drawLine(i, num);
  if (0 < i) drawEllipse(i);
}

void drawLine(int i, int k) {
  k--;
  if (dist(x[i], y[i], x[k], y[k]) < 20)
    line(x[i], y[i], x[k], y[k]);
  if (i < k) drawLine(i, k);
}