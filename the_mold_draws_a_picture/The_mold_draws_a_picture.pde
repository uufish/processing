void setup() {
  size(500, 500);
  frameRate(20);
  noStroke();
  fill(#89c3eb);
  smooth();
  background(#fbfaf5);
  init(num);
}

int num = 1;

float[]
  x  = new float [num], 
  y  = new float [num];

void init(int i) {
  i--;
  x[i] = width / 2 + random(-10, 10);
  y[i] = height / 2 + random(-10, 10);
  if (0 < i) init (i);
}

void draw() {
  fill(#fbfaf5, 10);
  rect(0, 0, width, height);
  drawEllipse(num);
}

void drawEllipse(int i) {
  i--;
  float r = random(100,255);
  float g = random(100,255);
  float b = random(180,255);
  fill(r, g, b, 180);
  x[i] *= x[i] < 0 || width < x[i] ? -1 : 1;
  y[i] *= y[i] < 0 || height < y[i] ? -1 : 1;
  int k = int(random(num-1));
  x[i] = x[k] + random(-8, 8);
  y[i] = y[k] + random(-8, 8);
  
  if (num < 500) appendEllipse();
  int size = int(random(2, 14));
  ellipse(x[i], y[i], size, size);
  if (0 < i) drawEllipse(i);
}

void appendEllipse() {
  if (random(1) < 0.2) {
    int k = int (random(num - 1));
    num++;
    x = (float[])append(x, width);
    y = (float[])append(y, height);
    x[num-1] = x[k] + random(-20, 20);
    y[num-1] = y[k] + random(-20, 20);
  }
}