import processing.opengl.*;
 
void setup () {
  size(500, 500, P3D);
  frameRate(30);
  smooth();
  stroke(0);
  strokeWeight(1);
  noFill();
  background(255);
  rectMode(CENTER);
  initSph();
}
   
int num = 60;
 
int[][] sph = new int[num][3];
 
void initSph () {
  for (int i=0; i<num; ++i) {   
    for (int k=0; k<3; ++k) {
      sph[i][k] = int(random(-width/2, width/2));
    }
  }
}
 
float[] rot = new float[3];
 
void draw() {
  background(255);
  if (360<=rot[0]) rot[0]=0;
  if (360<=rot[1]) rot[1]=0;
  translate(width/2, height/2, 0);
  if (width/2<mouseX) rot[0]++;
  if (mouseX<width/2) rot[0]--;
  if (height/2<mouseY) rot[1]++;
  if (mouseY<height/2) rot[1]--;
  rotateY(radians(rot[0]));
  rotateX(radians(rot[1]));
  for (int i=0; i<num; ++i) {
    pushMatrix();
    translate(sph[i][0], sph[i][1], sph[i][2]);
    stroke(0);
    fill(255);
    box(50);
    popMatrix();
  }
}